import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  constructor(private http: HttpClient) {}

  TestGet(): Observable<any> {
    return this.http.get<any>('http://localhost:8080/test')
  }

  GetFile(): Observable<any> {
    return this.http.get('http://localhost:8080/file', { responseType: 'arraybuffer' })
  }
}
