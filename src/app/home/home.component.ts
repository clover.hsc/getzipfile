import { Component, OnInit } from '@angular/core'
import { HomeService } from '../home.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(private httpServ: HomeService) {}

  ngOnInit(): void {}

  Test(): void {
    this.httpServ.TestGet().subscribe((res) => console.log(res))
  }

  GetFile(): void {
    this.httpServ.GetFile().subscribe((res) => {
      const blob = new Blob([res], { type: 'application/zip' })
      const url = window.URL.createObjectURL(blob)
      window.open(url)
    })
  }
}
